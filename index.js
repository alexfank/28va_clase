const express = require('express');
const app = express();

//Realizamos el llamado a la base datos
require('./src/database/db');
require('./src/database/sync');

//Configuramos nuestras rutas

const apiRouter = require('./src/routes/api')

//Configuramos para codificar JSON data
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(3000,()=>{
    console.log('Servidor corriendo por el puerto 3000');
})