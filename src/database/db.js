require('dotenv').config();

const {Sequelize} = require('sequelize');
const {database} = require('../config');

console.log(database.database);
console.log(database.username);
console.log(database.host);

const databaseAcamicadb = new Sequelize('acamicadb', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',
});

async function validar_conexion() {
    try {
        await databaseAcamicadb.authenticate();
        console.log('Conexión ha sido establecida correctamente.')
    } catch (error) {
        console.error('Error al conectarse a la base de datos:', error);
    }
}

validar_conexion();

module.exports = {
    databaseAcamicadb
}