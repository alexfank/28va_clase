const {Sequelize} = require('sequelize');
const {databaseAcamicadb} = require('./db');
const FrutaModelo = require('../models/frutas');

const Frutas = FrutaModelo(databaseAcamicadb, Sequelize);

databaseAcamicadb.sync({force:false}).then(() => {
    console.log('Tablas sincronizadas.');
});

module.exports = {
    Frutas
}