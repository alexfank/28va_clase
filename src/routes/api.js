const router = require('express').Router();

const apiFrutas = require('./api/frutas/');
const apiProductos = require('./api/productos/');
const apiUsuarios = require('./api/usuarios/');

router.use('/frutas',apiFrutas);
router.use('/productos',apiProductos);
router.use('/usuarios',apiUsuarios);

module.exports = router;